import { TipoComprobante } from './tipocomprobante';

export class Comprobante{
    idcomprobante:number;
    fecha:Date;
    ruc:string;
    consumidor:string;
    detalleordenF:any;
    numero:number;
    total:number;
    descuento:number;
    estado:number;
    serie:number;
    tipo:number;
    serieref:number;
    tiporef:number;
    totalref:number;
    fecharef:Date;
    numeroref:number;
    tipocomprobante: TipoComprobante;
}
