import { Producto } from './producto';
import { Comprobante } from './comprobante';

export class DetalleComprobante{
    iddetalleordenf:number;
    cantidad:number;
    producto:Producto;
    importe:number;
    comprobante:Comprobante; 
}