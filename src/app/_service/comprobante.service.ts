import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Comprobante } from '../_model/comprobante';
import { HOST, TOKEN_NAME } from '../_shared/var.constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ComprobanteService {

  ComprobanteCambio= new Subject<Comprobante[]>();
  mensaje = new Subject<string>();
  url: string=`${HOST}/comprobante`;
  
  constructor(private http:HttpClient) { }

  listarOrdenFarmacia(){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Comprobante[]>(this.url, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  listarOrdenFarmaciaId(id:number){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Comprobante>(`${this.url}/ultimo/${id}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    }) 
  }

  registrarOrdenFarmacia(comprobante: Comprobante){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(this.url, comprobante, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  modificarOrdenFarmacia(comprobante: Comprobante){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.put(this.url, comprobante, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  // reporteOrdenFPaciente(id: number) {    
  //   let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
  //   return this.http.get(`${this.url}/reporteOrdenF/${id}`, {
  //     responseType: 'blob',
  //     headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  //   });
  // }

  // reporteDetalladoFarmacia(fecha: string) {    
  //   let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
  //   return this.http.get(`${this.url}/reporteDetallado/${fecha}`, {
  //     responseType: 'blob',
  //     headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  //   });
  // }

  // reporteDetalladoFarmaciaXLSX(fecha: string) {    
  //   let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
  //   return this.http.get(`${this.url}/reporteDetalladoxlsx/${fecha}`, {
  //     responseType: 'blob',
  //     headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  //   });
  // }


  // buscarOrdenFarmaciaVigente(numeroorden:number){
  //   let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
  //   return this.http.get<OrdenFarmacia>(`${HOST}/ordenfarmacia/vigente/${numeroorden}`, {
  //     headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  //   }) 
  // }


  // reporteNCFarmaciaPDF(fechainicio: string,fechafin: string) {    
  //   let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
  //   return this.http.get(`${this.url}/reporteNCFarmacia/${fechainicio}/${fechafin}`, {
  //     responseType: 'blob',
  //     headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  //   });
  // }

  // reporteNCFarmaciaXLSX(fechainicio: string,fechafin: string) {    
  //   let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
  //   return this.http.get(`${this.url}/reporteNCFarmaciaxlsx/${fechainicio}/${fechafin}`, {
  //     responseType: 'blob',
  //     headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  //   });
  // }
}
