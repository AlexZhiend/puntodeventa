import { Injectable } from '@angular/core';
import { Producto } from '../_model/producto';
import { Subject } from 'rxjs';
import { HOST, TOKEN_NAME } from '../_shared/var.constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  productoCambio = new Subject<Producto[]>();
  mensaje = new Subject<string>();  
  url: string = `${HOST}/producto`;

  constructor(private http:HttpClient) { }

  listarproductos(){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Producto[]>(this.url, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  listarproductoporNombre(id: number){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Producto>(`${this.url}/${id}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  registrarproducto(producto:Producto){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(this.url, producto, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  modificarproducto(producto:Producto){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.put(this.url, producto, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  // reporteProductoGeneral() {    
  //   let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
  //   return this.http.get(`${this.url}/reporteProducto`, {
  //     responseType: 'blob',
  //     headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  //   });
  // }

  // reporteProductoVentas(fechainicio: string,fechafin:string) {    
  //   let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
  //   return this.http.get(`${this.url}/reporteProductoVentas/${fechainicio}/${fechafin}`, {
  //     responseType: 'blob',
  //     headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  //   });
  // }

  // reporteProductoVentasXls(fechainicio: string,fechafin:string) {    
  //   let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
  //   return this.http.get(`${this.url}/reporteExcelVentas/${fechainicio}/${fechafin}`, {
  //     responseType: 'blob',
  //     headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  //   });
  // }

  // reporteProductoXls() {    
  //   let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
  //   return this.http.get(`${this.url}/reporteExcelProducto`, {
  //     responseType: 'blob',
  //     headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  //   });
  // }

  buscarproductoporNombre(nombre: string){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Producto[]>(`${this.url}/buscarpornombre/${nombre}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  buscarproductoporCodigo(codigo: string){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Producto[]>(`${this.url}/buscarporcodigo/${codigo}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

}
