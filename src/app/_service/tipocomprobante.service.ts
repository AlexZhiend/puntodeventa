import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TipoComprobante } from '../_model/tipocomprobante';
import { HOST, TOKEN_NAME } from '../_shared/var.constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TipocomprobanteService {

  tipocomprobanteCambio = new Subject<TipoComprobante[]>();
  mensaje = new Subject<string>();
  url: string=`${HOST}/tipocomprobante`;

  constructor(private http: HttpClient) { }
  listartipocomprobante(){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<TipoComprobante[]>(this.url, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    })
  }

  listartipocomprobanteId(id: number){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<TipoComprobante>(`${this.url}/${id}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  registrartipocomprobante(tipoComprobante: TipoComprobante){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(this.url, tipoComprobante, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  modificartipocomprobante(tipoComprobante: TipoComprobante){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.put(this.url, tipoComprobante, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
}
