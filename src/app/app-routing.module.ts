import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { Not403Component } from './pages/not403/not403.component';
import { CajaComponent } from './pages/caja/caja.component';
import { CategoriaComponent } from './pages/categoria/categoria.component';
import { PresentacionComponent } from './pages/presentacion/presentacion.component';
import { ProductoComponent } from './pages/producto/producto.component';
import { ProveedorComponent } from './pages/proveedor/proveedor.component';
import { ReportesComponent } from './pages/reportes/reportes.component';
import { GuardService } from './_service/guard.service';
import { TipocomprobanteComponent } from './pages/tipocomprobante/tipocomprobante.component';

const routes: Routes = [  
  {path:"caja", component: CajaComponent},
  {path:'categoriaproducto', component: CategoriaComponent,canActivate:[GuardService] },
  {path:'presentacion', component: PresentacionComponent,canActivate:[GuardService] },
  {path:"producto", component: ProductoComponent,canActivate:[GuardService]},
  {path:'proveedor', component: ProveedorComponent,canActivate:[GuardService] },
  {path:'reportes', component: ReportesComponent,canActivate:[GuardService] },
  {path:'tipocomprobante', component: TipocomprobanteComponent,canActivate:[GuardService] },
  {path:"login", component: LoginComponent},
  {path:'not-403', component: Not403Component},
  {path: "", redirectTo:'login', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
