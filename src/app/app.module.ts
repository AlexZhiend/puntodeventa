import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { Not403Component } from './pages/not403/not403.component';
import { MaterialModule } from './material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PresentacionComponent } from './pages/presentacion/presentacion.component';
import { CajaComponent } from './pages/caja/caja.component';
import { ProductoComponent } from './pages/producto/producto.component';
import { ReportesComponent } from './pages/reportes/reportes.component';
import { ProveedorComponent } from './pages/proveedor/proveedor.component';
import { CategoriaComponent } from './pages/categoria/categoria.component';
import { ModalcategoriaComponent } from './pages/categoria/modalcategoria/modalcategoria.component';
import { ModalpresentacionComponent } from './pages/presentacion/modalpresentacion/modalpresentacion.component';
import { ModalproveedorComponent } from './pages/proveedor/modalproveedor/modalproveedor.component';
import { ModalproductoComponent } from './pages/producto/modalproducto/modalproducto.component';
import { TipocomprobanteComponent } from './pages/tipocomprobante/tipocomprobante.component';
import { ModaltipoComponent } from './pages/tipocomprobante/modaltipo/modaltipo.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    Not403Component,
    PresentacionComponent,
    CajaComponent,
    ProductoComponent,
    ReportesComponent,
    ProveedorComponent,
    CategoriaComponent,
    ModalcategoriaComponent,
    ModalpresentacionComponent,
    ModalproveedorComponent,
    ModalproductoComponent,
    TipocomprobanteComponent,
    ModaltipoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents:[ModalcategoriaComponent, ModalpresentacionComponent,ModalproveedorComponent,
  ModalproductoComponent,ModaltipoComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
