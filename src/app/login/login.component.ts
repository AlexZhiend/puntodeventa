import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TOKEN_NAME } from '../_shared/var.constant';
import * as decode from 'jwt-decode';
import { LoginService } from '../_service/login.service';
import { MenuService } from '../_service/menu.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: string;
  clave: string;

  //webSocketAPI: WebsocketAPI;
  greeting: any;
  name: string;

  constructor(private loginService: LoginService, private router: Router,  private menuService: MenuService) { }

  ngOnInit() {
    // this.webSocketAPI = new WebsocketAPI(new LoginComponent(this.loginService,this.router,this.menuService));
  }



  iniciarSesion(){
    this.loginService.login(this.usuario, this.clave).subscribe(data=>{
      //console.log(data);
      if (data) {
        let token = JSON.stringify(data);
        sessionStorage.setItem(TOKEN_NAME, token);

        let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
        const decodedToken = decode(tk.access_token);
        
        this.menuService.listarPorUsuario(decodedToken.user_name).subscribe(data => {
          this.menuService.menuCambio.next(data);
        });
        sessionStorage.setItem("nombre",this.usuario);
        
        let roles = decodedToken.authorities;
        for (let i = 0; roles.length; i++) {
          let rol = roles[i];
          if (rol === 'Administrador') {
            this.router.navigate(['producto']);
            break;  
          } else {
            if (rol === 'Caja') {
              this.router.navigate(['caja']);
              break;  
            }
          }
        }
      }

    });


  }

  // connect(){
  //   this.webSocketAPI._connect();
  // }

  // disconnect(){
  //   this.webSocketAPI._disconnect();
  // }

  // sendMessage(){
  //   this.webSocketAPI._send(this.name);
  // }

  // handleMessage(message){
  //   this.greeting = message;
  // }


}
