import { ProductoService } from 'src/app/_service/producto.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ComprobanteService } from 'src/app/_service/comprobante.service';
import { TipocomprobanteService } from 'src/app/_service/tipocomprobante.service';
import { Producto } from 'src/app/_model/producto';
import { MatTableDataSource, MatDialog, MatSnackBar, MatSelectChange, MatSort, MatPaginator } from '@angular/material';
import { DetalleComprobante } from 'src/app/_model/detallecomprobante';
import { Comprobante } from 'src/app/_model/comprobante';
import { TipoComprobante } from 'src/app/_model/tipocomprobante';
import { DataSource } from '@angular/cdk/table';

@Component({
  selector: 'app-caja',
  templateUrl: './caja.component.html',
  styleUrls: ['./caja.component.css']
})
export class CajaComponent implements OnInit {



  //tabla de productos
  dataSourceProd:MatTableDataSource<Producto>;
  displayedColumns2: string[] = ['codigoproducto', 'nombreproducto','cantidadproducto','pventaproducto','marcaproducto', 'presentacionproducto','presentacionproductounidad','presentacionproductocant','acciones'];
  productos: Producto[] = [];
  codigoing: string;
  nombreing:  string;
  cantidading:number=1;
  ingreso:number=0;
  vuelto:number=0;

  form: FormGroup;

  ultimaorden:Comprobante;
  numeroorden:string;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();


  displayedColumns: string[] = ['cantidaddetalle', 'denominacionproducto','presentacion','pventaproducto','importedetalle', 'acciones'];
  detalleComprobante: DetalleComprobante[] = [];
  dataSource: MatTableDataSource<DetalleComprobante>;

  productoseleccionado: Producto;

  tiposcomp:TipoComprobante[]=[];
  tipoSeleccionado:TipoComprobante;
  nombrecargado:string;
  total:number=0;

  comprobante: Comprobante;

 @ViewChild("name") nameField: ElementRef;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private comprobanteService: ComprobanteService,
    private tipocomprobanteService: TipocomprobanteService,
    private productoservice: ProductoService,
    public dialog:MatDialog,
    public snackBar:MatSnackBar) {
    
      this.form= new FormGroup({
        'numeroorden': new FormControl({value:0 , disabled:true}, Validators.required),
        'rucordenfarmacia': new FormControl(''),
        'fechaseleccionada': new FormControl(new Date),
        'consumidorordenfarmacia': new FormControl(''),
        'idtipoSeleccionado':new FormControl('Boleta'),
      });
      this.productoseleccionado= new Producto();
      this.total=0;
      this.comprobante=new Comprobante();
      this.tipoSeleccionado= new TipoComprobante();
               }

  ngOnInit() {

    this.companterior();
    this.listarTipos();
    this.tipoSeleccionado.idtipocomprobante=1;

    this.comprobanteService.mensaje.subscribe(data=>{
      this.snackBar.open(data, null, { duration: 4000 });
      });

  }


  listarTipos(){
    this.tipocomprobanteService.listartipocomprobante().subscribe(data=>{
      this.tiposcomp=data;
    })
  }

  companterior(){
    this.comprobanteService.listarOrdenFarmaciaId(1).subscribe(data=>{
      this.ultimaorden=data;
      console.log(this.ultimaorden);
      if(this.ultimaorden!=null){
      var numero= this.ultimaorden.numero + 1;
      }else{
        numero=1;
      }

      let numeronuevo=numero.toString();
      let cero = "0";
      for(var _i=0;_i < 7-numeronuevo.length; _i++){
        cero=cero+"0";
      }
      this.numeroorden=cero+numeronuevo;
      console.log(parseInt(this.numeroorden));
    });
  }



  operar(){
    if (this.detalleComprobante.length >0) {

      let tipo = new TipoComprobante();
      tipo=this.tipoSeleccionado;
      console.log(tipo);
      
      this.comprobante.numero=parseInt(this.numeroorden);
      this.comprobante.ruc=this.form.value['rucordenfarmacia'];
      this.comprobante.fecha=this.form.value['fechaseleccionada'];
      this.comprobante.consumidor=this.form.value['consumidorordenfarmacia'];
      this.comprobante.tipocomprobante=tipo;
      this.comprobante.estado=1;
      this.comprobante.serie=1;
      this.comprobante.total=this.total;
      this.comprobante.detalleordenF=this.detalleComprobante;
      console.log(this.comprobante)

      this.comprobanteService.registrarOrdenFarmacia(this.comprobante).subscribe(data =>{
        this.comprobanteService.mensaje.next("Se realizó el registro con éxito");
        console.log(data);
        // this.comprobantepagoService.reporteComprobanteU(parseInt(this.numerocomprobante)).subscribe(data=>{
        //   var data2 = new Blob([data],{type:'application/pdf;charset=utf-8"'});
        //   var fileURL = window.URL.createObjectURL(data2);
        //   window.open(fileURL,"_blank");
        // });
      })
    } else {
      this.comprobanteService.mensaje.next('No se puede procesar la operación, revise el registro');
    }

  }


  getTotalCost() {
    this.total=this.detalleComprobante.map(t => t.importe).reduce((acc, value) => acc + value, 0);
    return this.detalleComprobante.map(t => t.importe).reduce((acc, value) => acc + value, 0);
  }

  eliminar(transaction){
    var indice=this.detalleComprobante.indexOf(transaction);
    this.detalleComprobante.splice(indice, 1);
    this.dataSource = new MatTableDataSource(this.detalleComprobante);
    this.nameField.nativeElement.focus();
    this.nombrecargado=null;
  }

  cancelar(){
    this.comprobanteService.mensaje.next('Se canceló el procedimiento');
    window.location.reload();
  }

  imprimir(){

  }

  tipocomprobante(event: MatSelectChange) {
      this.tipoSeleccionado=event.source.value;
      console.log(this.tipoSeleccionado)
      this.comprobanteService.listarOrdenFarmaciaId(this.tipoSeleccionado.idtipocomprobante).subscribe(data=>{
        this.ultimaorden=data;
        console.log(this.ultimaorden);
        if(this.ultimaorden!=null){
        var numero= this.ultimaorden.numero + 1;
        }else{
          numero=1;
        }
  
        let numeronuevo=numero.toString();
        let cero = "0";
        for(var _i=0;_i < 7-numeronuevo.length; _i++){
          cero=cero+"0";
        }
        this.numeroorden=cero+numeronuevo;
        console.log(parseInt(this.numeroorden));
      });
  }

  buscarPorNombre(){

    if (this.nombreing!=null) {
    this.productoservice.buscarproductoporNombre(this.nombreing).subscribe(data =>{
      
      this.productos=data;
      console.log(this.productos);
      this.dataSourceProd = new MatTableDataSource(this.productos);

      this.dataSourceProd.paginator = this.paginator;
      this.dataSourceProd.sort = this.sort;
    })
    } else {
      this.comprobanteService.mensaje.next("Ingrese el nombre del producto");
    }

  }

  buscarPorCodigo(){
    if (this.codigoing!=null) {
      this.productoservice.buscarproductoporCodigo(this.codigoing).subscribe(data =>{
        this.productos=data;
        
        this.dataSourceProd = new MatTableDataSource(this.productos);
  
        this.dataSourceProd.paginator = this.paginator;
        this.dataSourceProd.sort = this.sort;
      })
    } else {
      this.comprobanteService.mensaje.next("Ingrese el código del producto");
    }

  }

  agregar(row:Producto){
    this.productoseleccionado=row;
    this.nombrecargado=this.productoseleccionado.nombreproducto;
  }

  cargar(){
    if (this.cantidading > 0 && this.nombrecargado != null) {
      console.log(this.productoseleccionado);
      let det = new DetalleComprobante();
      det.cantidad= this.cantidading;
      det.producto= this.productoseleccionado;
      det.importe=this.cantidading*this.productoseleccionado.pventaproducto;

      this.detalleComprobante.push(det);
      this.dataSource = new MatTableDataSource(this.detalleComprobante);
      this.codigoing=null;
      this.nombreing=null;
      this.nameField.nativeElement.focus();
      this.nombrecargado=null;

      this.dataSourceProd=new MatTableDataSource;
    }
    else{
      this.comprobanteService.mensaje.next('Seleccione un producto y revise la cantidad');
    }
  }

}
