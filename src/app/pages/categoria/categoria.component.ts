import { Component, OnInit, ViewChild } from '@angular/core';
import { Categoriaproducto } from 'src/app/_model/categoriaproducto';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';
import { CategoriaproductoService } from 'src/app/_service/categoriaproducto.service';
import { ModalcategoriaComponent } from './modalcategoria/modalcategoria.component';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {

  lista: Categoriaproducto[]=[];
  displayedColumns=['idcategoriaproducto','nombrecategoriaproducto','acciones'];
  dataSource: MatTableDataSource<Categoriaproducto>;
  row:Categoriaproducto;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort:MatSort;

  constructor(private categoriaproductoservice:CategoriaproductoService,
              public dialog:MatDialog, 
              private matSnackBar:MatSnackBar) { }

  ngOnInit() {
    this.categoriaproductoservice.categoriaproductoCambio.subscribe(data => {
      this.lista=data;
      this.dataSource= new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      this.categoriaproductoservice.mensaje.subscribe(data =>{
        this.matSnackBar.open(data, 'Aviso',{duration:3000});
      });
    });


    this.categoriaproductoservice.listarCategoriaProducto().subscribe(data => {
      this.lista=data;
      console.log(data);
      this.dataSource=new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  applyFilter(filterValue: string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }


  openModal(categoria:Categoriaproducto){
    let pro = categoria != null ? categoria : new Categoriaproducto();
    let dialogRef = this.dialog.open(ModalcategoriaComponent, {
      width: '600px',   
      disableClose: true,   
      
      data: pro      
    });
  }
}
