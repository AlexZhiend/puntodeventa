import { Component, OnInit, Inject } from '@angular/core';
import { Categoriaproducto } from 'src/app/_model/categoriaproducto';
import { FormGroup, FormControl } from '@angular/forms';
import { CategoriaproductoService } from 'src/app/_service/categoriaproducto.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modalcategoria',
  templateUrl: './modalcategoria.component.html',
  styleUrls: ['./modalcategoria.component.css']
})
export class ModalcategoriaComponent implements OnInit {

  id:number;
  categoriaproducto:Categoriaproducto;
  form: FormGroup;
  edicion:boolean=false;
  estado:boolean=false;

  constructor(
    public dialogRef: MatDialogRef<ModalcategoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Categoriaproducto,
    private categoriaproductoService:CategoriaproductoService) {
              this.categoriaproducto=new Categoriaproducto;
             }

  ngOnInit() {
    this.categoriaproducto= new Categoriaproducto();
    this.categoriaproducto.idcategoriaproducto=this.data.idcategoriaproducto;
    this.categoriaproducto.nombrecategoriaproducto=this.data.nombrecategoriaproducto;
   }



  operar(){

    console.log(this.categoriaproducto)

    if(this.categoriaproducto != null && this.categoriaproducto.idcategoriaproducto != undefined ){
      this.categoriaproductoService.modificarCategoriaproducto(this.categoriaproducto).subscribe(data=>{
        console.log(data);
        this.categoriaproductoService.listarCategoriaProducto().subscribe(categoriasproducto =>{
          this.categoriaproductoService.categoriaproductoCambio.next(categoriasproducto);
          this.categoriaproductoService.mensaje.next('Se modificó la categoria correctamente');
          this.dialogRef.close();
        });
      });
    }
    else{
      this.categoriaproductoService.registrarCategoriaproducto(this.categoriaproducto).subscribe(data =>{
        console.log(data);
        this.categoriaproductoService.listarCategoriaProducto().subscribe(categoriasproducto => {
          this.categoriaproductoService.categoriaproductoCambio.next(categoriasproducto);
          this.categoriaproductoService.mensaje.next('Se registro la categoria correctamente');
          this.dialogRef.close();
        });
      });
    }

  }

  cerrar(){
    this.dialogRef.close();
  }

}
