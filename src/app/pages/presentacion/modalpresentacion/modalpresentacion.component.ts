import { Component, OnInit, Inject } from '@angular/core';
import { Presentacionproducto } from 'src/app/_model/presentacion';
import { PresentacionService } from 'src/app/_service/presentacion.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modalpresentacion',
  templateUrl: './modalpresentacion.component.html',
  styleUrls: ['./modalpresentacion.component.css']
})
export class ModalpresentacionComponent implements OnInit {

  id:number;
  presentacionproducto:Presentacionproducto;
  edicion:boolean=false;
  estado:boolean=false;

  constructor(
    public dialogRef: MatDialogRef<ModalpresentacionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Presentacionproducto,
    private presentacionService:PresentacionService) {
              this.presentacionproducto=new Presentacionproducto;
              }

  ngOnInit() {
    this.presentacionproducto=new Presentacionproducto();
    this.presentacionproducto.idpresentacion=this.data.idpresentacion;
    this.presentacionproducto.nombre=this.data.nombre;
    this.presentacionproducto.unidad=this.data.unidad;
    this.presentacionproducto.cantidad=this.data.cantidad;
  }

  operar(){

    if(this.presentacionproducto != null && this.presentacionproducto.idpresentacion != null){
      this.presentacionService.modificarPresentacionproducto(this.presentacionproducto).subscribe(data=>{
        console.log(data);
        this.presentacionService.listarPresentacion().subscribe(presentaciones =>{
          this.presentacionService.presentacionCambio.next(presentaciones);
          this.presentacionService.mensaje.next('Se modificó la presentación correctamente');
          this.dialogRef.close();
        });
      });
    }
    else{
      this.presentacionService.registrarPresentacionproducto(this.presentacionproducto).subscribe(data =>{
        console.log(data);
        this.presentacionService.listarPresentacion().subscribe(presentaciones => {
          this.presentacionService.presentacionCambio.next(presentaciones);
          this.presentacionService.mensaje.next('Se registro la presentación correctamente');
          this.dialogRef.close();
        });
      });
    }

  }

  cerrar(){
    this.presentacionService.mensaje.next('Se canceló el registro');
    this.dialogRef.close();
  }

}
