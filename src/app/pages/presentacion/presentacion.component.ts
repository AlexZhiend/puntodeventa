import { Component, OnInit, ViewChild } from '@angular/core';
import { Presentacionproducto } from 'src/app/_model/presentacion';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';
import { PresentacionService } from 'src/app/_service/presentacion.service';
import { ModalpresentacionComponent } from './modalpresentacion/modalpresentacion.component';

@Component({
  selector: 'app-presentacion',
  templateUrl: './presentacion.component.html',
  styleUrls: ['./presentacion.component.css']
})
export class PresentacionComponent implements OnInit {

  lista: Presentacionproducto[]=[];
  displayedColumns=['idpresentacionproducto','nombrepresentacionproducto','cantidadpresentacionproducto','unidadpresentacionproducto','acciones'];
  dataSource: MatTableDataSource<Presentacionproducto>;

  row:Presentacionproducto;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort:MatSort;

  constructor(private presentacionservice:PresentacionService,
    public dialog:MatDialog, 
    private matSnackBar:MatSnackBar) { }

  ngOnInit() {
    this.presentacionservice.presentacionCambio.subscribe(data=>{
      this.lista=data;
      this.dataSource= new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      this.presentacionservice.mensaje.subscribe(data => {
        this.matSnackBar.open(data,'Aviso', {duration:3000});
      });
    });


    this.presentacionservice.listarPresentacion().subscribe(data => {
      this.lista=data;
      console.log(data);
      this.dataSource=new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  applyFilter(filterValue: string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }

  
  openModal(presentacion:Presentacionproducto){
    let pro = presentacion != null ? presentacion : new Presentacionproducto();
    let dialogRef = this.dialog.open(ModalpresentacionComponent, {
      width: '600px',   
      disableClose: true,   
      
      data: pro      
    });
  }

}
