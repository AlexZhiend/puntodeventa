import { Component, OnInit, Inject } from '@angular/core';
import { Producto } from 'src/app/_model/producto';
import { Proveedor } from 'src/app/_model/proveedor';
import { Categoriaproducto } from 'src/app/_model/categoriaproducto';
import { MatDialogRef, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent } from '@angular/material';
import { Presentacionproducto } from 'src/app/_model/presentacion';
import { FormGroup, FormControl,ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { ProductoService } from 'src/app/_service/producto.service';
import { ProveedorService } from 'src/app/_service/proveedor.service';
import { CategoriaproductoService } from 'src/app/_service/categoriaproducto.service';
import { PresentacionService } from 'src/app/_service/presentacion.service';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-modalproducto',
  templateUrl: './modalproducto.component.html',
  styleUrls: ['./modalproducto.component.css']
})
export class ModalproductoComponent implements OnInit {

  producto: Producto;
  proveedores: Proveedor[] = [];
  categorias: Categoriaproducto[] = [];
  presentaciones: Presentacionproducto[] = [];
  idProveedorSeleccionado: number;
  idCategoriaSeleccionado: number;
  idPresentacionSeleccionado: number;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  form: FormGroup;

  present = new FormControl();
  filteredOptions: Observable<Presentacionproducto[]>;
  presentacion = new Presentacionproducto();

  constructor(public dialogRef: MatDialogRef<ModalproductoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Producto,
    private productoService: ProductoService,
    private proveedorService: ProveedorService,
    private categoriaproductoService: CategoriaproductoService,
    private presentacionService: PresentacionService) {
    this.form = new FormGroup({
      'present': new FormControl(''),
    });
  }

  ngOnInit() {
    this.listarProveedores();
    this.listarCategorias();
    this.listarPresentaciones();
    this.producto = new Producto();
    this.producto.idproducto = this.data.idproducto;
    this.producto.codigo = this.data.codigo;
    this.producto.nombreproducto = this.data.nombreproducto;
    this.producto.fvproducto = this.data.fvproducto;
    this.producto.cantidadproducto = this.data.cantidadproducto;
    this.producto.pventaproducto = this.data.pventaproducto;
    this.producto.pingresoproducto = this.data.pingresoproducto;
    this.producto.fingresoproducto = this.data.fingresoproducto;
    this.producto.marcaproducto = this.data.marcaproducto;
    this.producto.proveedor = this.data.proveedor;
    this.producto.categoriaproducto = this.data.categoriaproducto;
    this.producto.presentacionproducto = this.data.presentacionproducto;
    if (this.data.idproducto != null) {
      this.present.setValue(this.data.presentacionproducto)
      this.idProveedorSeleccionado = this.data.proveedor.idproveedor;
      this.idCategoriaSeleccionado = this.data.categoriaproducto.idcategoriaproducto;
      this.idPresentacionSeleccionado = this.data.presentacionproducto.idpresentacion;
    }


    this.filteredOptions = this.present.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.nombre),
      map(nombre => nombre ? this._filter(nombre) : this.presentaciones.slice())
    );


  }


  listarProveedores() {
    this.proveedorService.listarProveedor().subscribe(data => {
      this.proveedores = data;
    });
  }
  listarCategorias() {
    this.categoriaproductoService.listarCategoriaProducto().subscribe(data => {
      this.categorias = data;
    });
  }

  listarPresentaciones() {
    this.presentacionService.listarPresentacion().subscribe(data => {
      this.presentaciones = data;
    });
  }

  displayFn(presentacion?: Presentacionproducto): string | undefined {
    return presentacion ? presentacion.nombre : undefined;
  }

  private _filter(nombrepresentacionproducto: string): Presentacionproducto[] {
    const filterValue = nombrepresentacionproducto.toLowerCase();
    return this.presentaciones.filter(pre => pre.nombre.toLowerCase().indexOf(filterValue) === 0);
  }

  onSelectionChanged(event: MatAutocompleteSelectedEvent) {
    this.presentacion = event.option.value;
    this.idPresentacionSeleccionado = this.presentacion.idpresentacion;
    console.log(this.idPresentacionSeleccionado)
  }


  operar() {

    if (this.producto != null && this.producto.idproducto > 0) {

      let proveedor = new Proveedor();
      proveedor.idproveedor = this.idProveedorSeleccionado;
      let categoria = new Categoriaproducto();
      categoria.idcategoriaproducto = this.idCategoriaSeleccionado;
      let presentacion = new Presentacionproducto();
      presentacion.idpresentacion = this.idPresentacionSeleccionado;

      this.producto.proveedor = proveedor;
      this.producto.categoriaproducto = categoria;
      this.producto.presentacionproducto = presentacion;

      this.productoService.modificarproducto(this.producto).subscribe(data => {

        this.productoService.listarproductos().subscribe(productos => {
          this.productoService.productoCambio.next(productos);
          this.productoService.mensaje.next("Se modificó correctamente");
        });
        this.dialogRef.close();
      });
    } else {

      let proveedor = new Proveedor();
      proveedor.idproveedor = this.idProveedorSeleccionado;
      let categoria = new Categoriaproducto();
      categoria.idcategoriaproducto = this.idCategoriaSeleccionado;
      let presentacion = new Presentacionproducto();
      presentacion.idpresentacion = this.idPresentacionSeleccionado;
      this.producto.proveedor = proveedor;
      this.producto.categoriaproducto = categoria;
      this.producto.presentacionproducto = presentacion;

      if (this.producto.nombreproducto != null && this.producto.cantidadproducto != null && this.producto.pventaproducto != null && this.producto.pingresoproducto != null
        && this.producto.marcaproducto != null && this.producto.fingresoproducto != null && this.producto.presentacionproducto != null && this.producto.proveedor != null
        && this.producto.categoriaproducto != null) {

        this.productoService.registrarproducto(this.producto).subscribe(data => {

          this.productoService.listarproductos().subscribe(productos => {
            this.productoService.productoCambio.next(productos);
            this.productoService.mensaje.next("Se registró correctamente");
          });

        });
        this.dialogRef.close();

      } else {

        this.productoService.mensaje.next('Falta algún dato requerido del producto');
      }

    }

  }

  cancelar() {
    this.dialogRef.close();
    this.productoService.mensaje.next('Se canceló el procedimiento');
  }

}
