import { Component, OnInit, inject, Inject } from '@angular/core';
import { Proveedor } from 'src/app/_model/proveedor';
import { ProveedorService } from 'src/app/_service/proveedor.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-modalproveedor',
  templateUrl: './modalproveedor.component.html',
  styleUrls: ['./modalproveedor.component.css']
})
export class ModalproveedorComponent implements OnInit {

  
  id:number;
  proveedor:Proveedor;
  estado:boolean=false;
  

  constructor(
    public dialogRef: MatDialogRef<ModalproveedorComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: Proveedor,
    private proveedorService:ProveedorService) {
}

  ngOnInit() {

    this.proveedor= new Proveedor();
    this.proveedor.idproveedor=this.data.idproveedor;
    this.proveedor.correoproveedor=this.data.correoproveedor;
    this.proveedor.desripcionproveedor=this.data.desripcionproveedor;
    this.proveedor.direccionproveedor=this.data.direccionproveedor;
    this.proveedor.nombreproveedor=this.data.nombreproveedor;
    this.proveedor.telefonoproveedor=this.data.telefonoproveedor;
  }

  // private initForm() {
  //   if(this.edicion){
  //     this.proveedorService.listarProveedorPorId(this.id).subscribe(data=>{
  //       let id = data.idproveedor;
  //       let nombre = data.nombreproveedor;
  //       let telefono = data.telefonoproveedor;
  //       let direccion=data.direccionproveedor;
  //       let correo=data.correoproveedor;
  //       let descripcion=data.desripcionproveedor; 

  //       this.form= new FormGroup({
  //         'id': new FormControl(id),
  //         'nombre': new FormControl(nombre),
  //         'telefono':new FormControl(telefono),
  //         'direccion':new FormControl(direccion),
  //         'correo':new FormControl(correo),
  //         'descripcion':new FormControl(descripcion),
  //       });
  //     });
  //   }
  // }

  operar(){
    console.log(this.proveedor);
    if(this.proveedor != null && this.proveedor.idproveedor != null){
      console.log(this.proveedor);
      if (this.proveedor.nombreproveedor!= "") {
        this.proveedorService.modificarProveedor(this.proveedor).subscribe(data=>{
          console.log(data);
          this.proveedorService.listarProveedor().subscribe(proveedores =>{
            this.proveedorService.proveedorCambio.next(proveedores);
            this.proveedorService.mensaje.next('Se modificó el proveedor correctamente');
            this.dialogRef.close();
          });
        });
        this.estado=true;
      } else {
        this.proveedorService.mensaje.next('Necesita ingresar el nombre del proveedor');
      }
    }
    else{
      console.log(this.proveedor);
      if (this.proveedor.nombreproveedor!= undefined) {
        this.proveedorService.registrarProveedor(this.proveedor).subscribe(data =>{
          console.log(data);
          this.proveedorService.listarProveedor().subscribe(proveedores => {
            this.proveedorService.proveedorCambio.next(proveedores);
            this.proveedorService.mensaje.next('Se registro el proveedor correctamente');
            this.dialogRef.close();
          });
        });
        this.estado=true;
      } else {
        this.proveedorService.mensaje.next('Necesita ingresar el nombre del proveedor');
      }
      
    }

  }

  cerrar(){
    this.dialogRef.close();
  }

}
