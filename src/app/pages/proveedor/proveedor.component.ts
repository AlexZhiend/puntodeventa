import { Component, OnInit, ViewChild } from '@angular/core';
import { Proveedor } from 'src/app/_model/proveedor';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';
import { ProveedorService } from 'src/app/_service/proveedor.service';
import { ModalproveedorComponent } from './modalproveedor/modalproveedor.component';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styleUrls: ['./proveedor.component.css']
})
export class ProveedorComponent implements OnInit {


  lista: Proveedor[]=[];
  displayedColumns=['idproveedor','nombreproveedor','descripcionproveedor','telefonoproveedor','direccionproveedor','correoproveedor','acciones'];
  dataSource: MatTableDataSource<Proveedor>;
  row : Proveedor;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort:MatSort;

  constructor(private proveedorService:ProveedorService,
    public dialog:MatDialog, 
    private matSnackBar:MatSnackBar) {
   }
   
    ngOnInit() {
      this.proveedorService.proveedorCambio.subscribe(data => {
        this.lista=data;
        this.dataSource= new MatTableDataSource(this.lista);
        this.dataSource.paginator=this.paginator;
        this.dataSource.sort=this.sort;

        this.proveedorService.mensaje.subscribe(data => {
          this.matSnackBar.open(data, 'Aviso',{duration:2000});
        });
      });

      this.proveedorService.listarProveedor().subscribe(data => {
        this.lista=data;
        console.log(data);
        this.dataSource=new MatTableDataSource(this.lista);
        this.dataSource.paginator=this.paginator;
        this.dataSource.sort=this.sort;
      });

    }

    applyFilter(filterValue: string){
      filterValue=filterValue.trim();
      filterValue=filterValue.toLowerCase();
      this.dataSource.filter=filterValue;
    }


    openModal(proveedor:Proveedor){
      let pro = proveedor != null ? proveedor : new Proveedor();
      let dialogRef = this.dialog.open(ModalproveedorComponent, {
        width: '600px',   
        disableClose: true,   
        
        data: pro      
      });
    }

}
