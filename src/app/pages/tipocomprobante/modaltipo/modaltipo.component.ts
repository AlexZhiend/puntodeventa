import { Component, OnInit, Inject } from '@angular/core';
import { TipoComprobante } from 'src/app/_model/tipocomprobante';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TipocomprobanteService } from 'src/app/_service/tipocomprobante.service';

@Component({
  selector: 'app-modaltipo',
  templateUrl: './modaltipo.component.html',
  styleUrls: ['./modaltipo.component.css']
})
export class ModaltipoComponent implements OnInit {


  id:number;
  tipocomprobante:TipoComprobante;
  form: FormGroup;
  edicion:boolean=false;
  estado:boolean=false;

  constructor(
    public dialogRef: MatDialogRef<ModaltipoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TipoComprobante,
    private tipocomprobanteService:TipocomprobanteService) {
              this.tipocomprobante=new TipoComprobante();
             }

  ngOnInit() {
    this.tipocomprobante= new TipoComprobante();
    this.tipocomprobante.idtipocomprobante=this.data.idtipocomprobante;
    this.tipocomprobante.denominacion=this.data.denominacion;
   }



  operar(){

    console.log(this.tipocomprobante);

    if(this.tipocomprobante != null && this.tipocomprobante.idtipocomprobante != undefined ){
      this.tipocomprobanteService.modificartipocomprobante(this.tipocomprobante).subscribe(data=>{
        console.log(data);
        this.tipocomprobanteService.listartipocomprobante().subscribe(tipocomprobantes =>{
          this.tipocomprobanteService.tipocomprobanteCambio.next(tipocomprobantes);
          this.tipocomprobanteService.mensaje.next('Se modificó la categoria correctamente');
          this.dialogRef.close();
        });
      });
    }
    else{
      this.tipocomprobanteService.registrartipocomprobante(this.tipocomprobante).subscribe(data =>{
        console.log(data);
        this.tipocomprobanteService.listartipocomprobante().subscribe(tipocomprobantes => {
          this.tipocomprobanteService.tipocomprobanteCambio.next(tipocomprobantes);
          this.tipocomprobanteService.mensaje.next('Se registro la categoria correctamente');
          this.dialogRef.close();
        });
      });
    }

  }

  cerrar(){
    this.dialogRef.close();
  }

}
