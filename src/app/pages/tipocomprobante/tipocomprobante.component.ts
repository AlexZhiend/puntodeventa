import { Component, OnInit, ViewChild } from '@angular/core';
import { TipoComprobante } from 'src/app/_model/tipocomprobante';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { TipocomprobanteService } from 'src/app/_service/tipocomprobante.service';
import { ModaltipoComponent } from './modaltipo/modaltipo.component';

@Component({
  selector: 'app-tipocomprobante',
  templateUrl: './tipocomprobante.component.html',
  styleUrls: ['./tipocomprobante.component.css']
})
export class TipocomprobanteComponent implements OnInit {

  lista: TipoComprobante[]=[];
  displayedColumns=['idtipo','denominacion','acciones'];
  dataSource: MatTableDataSource<TipoComprobante>;
  row:TipoComprobante;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort:MatSort;

  constructor(private tipocomprobanteservice:TipocomprobanteService,
              public dialog:MatDialog, 
              private matSnackBar:MatSnackBar) { }

  ngOnInit() {
    this.tipocomprobanteservice.tipocomprobanteCambio.subscribe(data => {
      this.lista=data;
      this.dataSource= new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      this.tipocomprobanteservice.mensaje.subscribe(data =>{
        this.matSnackBar.open(data, 'Aviso',{duration:3000});
      });
    });


    this.tipocomprobanteservice.listartipocomprobante().subscribe(data => {
      this.lista=data;
      console.log(data);
      this.dataSource=new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  applyFilter(filterValue: string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }


  openModal(tipo:TipoComprobante){
    let pro = tipo != null ? tipo : new TipoComprobante();
    let dialogRef = this.dialog.open(ModaltipoComponent, {
      width: '600px',   
      disableClose: true,   
      
      data: pro      
    });
  }
}
